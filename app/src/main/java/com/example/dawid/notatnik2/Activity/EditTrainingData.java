package com.example.dawid.notatnik2.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.example.dawid.notatnik2.Models.IntentConstants;
import com.example.dawid.notatnik2.Models.Training;
import com.example.dawid.notatnik2.R;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EditTrainingData extends AppCompatActivity {

    @BindView(R.id.title_edit)
    EditText titleEdit;
    @BindView(R.id.date_of_training_edit)
    Button dateEdit;
    @BindView(R.id.distance_edit)
    Button distanceEdit;
    @BindView(R.id.time_of_route_edit)
    Button timeOfRouteEdit;
    @BindView(R.id.average_speed_edit)
    EditText averageSpeedEdit;
    @BindView(R.id.uphill_edit)
    EditText uphillEdit;
    @BindView(R.id.comment_edit)
    EditText commentsEdit;

    @BindView(R.id.spinner_average_edit)
    Spinner spinnerAverage;
    @BindView(R.id.spinner_uphill_edit)
    Spinner spinnerUphill;

    @BindView(R.id.save_but)
    Button editBut;
    @BindView(R.id.delete_but_edit)
    Button deleteBut;
    @BindView(R.id.back_but_edit)
    Button backBut;

    Intent intent;

    DatePickerDialog.OnDateSetListener dateSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_training_data);
        ButterKnife.bind(this);

        getDataOfEdit();
        averageAdapter();
        uphillAdapter();


        dateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateOfTrainingFormat();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                String date = year + "-" + month + "-" + day;
                dateEdit.setText(date);
            }
        };
        distanceEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDistance();
            }
        });
        timeOfRouteEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTime();
            }
        });


        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        editBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTrainingData();
            }
        });

        deleteBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog();
            }
        });
    }

    public void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete training")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setResult(IntentConstants.INTENT_RESULT_DELETE, intent);
                        supportFinishAfterTransition();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    public void averageAdapter() {
        ArrayAdapter<CharSequence> adapterAverage = ArrayAdapter.createFromResource(this,
                R.array.spinner_average, android.R.layout.simple_spinner_dropdown_item);
        adapterAverage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAverage.setAdapter(adapterAverage);
    }

    public void uphillAdapter() {
        ArrayAdapter<CharSequence> adapterUphill = ArrayAdapter.createFromResource(this,
                R.array.spinner_uphill, android.R.layout.simple_spinner_dropdown_item);
        adapterUphill.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUphill.setAdapter(adapterUphill);
    }

    public void dateOfTrainingFormat() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                EditTrainingData.this, android.R.style.Theme_Holo_Dialog_MinWidth,
                dateSetListener, year, month, day);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void getDataOfEdit() {
        intent = getIntent();

        dateEdit.setText(intent.getStringExtra(IntentConstants.INTENT_DATE));
        titleEdit.setText(intent.getStringExtra(IntentConstants.INTENT_TITLE));
        distanceEdit.setText(intent.getStringExtra(IntentConstants.INTENT_DISTANCE));
        timeOfRouteEdit.setText(intent.getStringExtra(IntentConstants.INTENT_TIME_OF_ROUTE));
        averageSpeedEdit.setText(intent.getStringExtra(IntentConstants.INTENT_AVERAGE_SPEED));
        uphillEdit.setText(intent.getStringExtra(IntentConstants.INTENT_UPHILL));
        commentsEdit.setText(intent.getStringExtra(IntentConstants.INTENT_COMMENTS));
    }

    private void editTrainingData() {
        Training trainingEdit = new Training();
        trainingEdit.setDate(dateEdit.getText().toString());
        trainingEdit.setTitle(titleEdit.getText().toString());
        trainingEdit.setDistance(distanceEdit.getText().toString());
        trainingEdit.setTimeOfRoute(timeOfRouteEdit.getText().toString());
        trainingEdit.setAverageSpeed(averageSpeedEdit.getText().toString());
        trainingEdit.setUphill(uphillEdit.getText().toString());
        trainingEdit.setComment(commentsEdit.getText().toString());

        intent = new Intent(this, ListActivity.class);
        intent.putExtra("trainingEdit", trainingEdit);
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showTime() {

        final Dialog dialog = new Dialog(EditTrainingData.this);
        dialog.setContentView(R.layout.dialog_time_format);
        final NumberPicker hourPicker = (NumberPicker) dialog.findViewById(R.id.hour);
        final NumberPicker minutePicker = (NumberPicker) dialog.findViewById(R.id.minute);
        final NumberPicker secondsPicker = (NumberPicker) dialog.findViewById(R.id.seconds);
        Button okTimeButton = (Button) dialog.findViewById(R.id.ok_time);
        Button cancelTimeButton = (Button) dialog.findViewById(R.id.cancel_time);
        hourPicker.setMaxValue(48);
        hourPicker.setMinValue(0);
        minutePicker.setMaxValue(59);
        minutePicker.setMinValue(0);
        secondsPicker.setMaxValue(59);
        secondsPicker.setMinValue(0);
        okTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timeOfRouteEdit.setText(String.valueOf(hourPicker.getValue())
                        + "h" + (minutePicker.getValue())
                        + " m" + (secondsPicker.getValue())
                        + " s");
                dialog.dismiss();
            }
        });

        cancelTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showDistance() {
        final Dialog dialog = new Dialog(EditTrainingData.this);
        dialog.setContentView(R.layout.dialog_distance_format);
        final NumberPicker distance = (NumberPicker) dialog.findViewById(R.id.distance);
        final NumberPicker distanceAfterComa = (NumberPicker) dialog.findViewById(R.id.distance_after_coma);
        final Spinner spinnerDistance = (Spinner) dialog.findViewById(R.id.spinner_distance);
        ArrayAdapter<CharSequence> adapterDistance = ArrayAdapter.createFromResource(this,
                R.array.spinner_distance, android.R.layout.simple_spinner_dropdown_item);
        adapterDistance.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistance.setAdapter(adapterDistance);
        Button okTimeButton = (Button) dialog.findViewById(R.id.ok_distance);
        Button cancelTimeButton = (Button) dialog.findViewById(R.id.cancel_distance);
        distance.setMaxValue(2000);
        distance.setMinValue(0);
        distance.setWrapSelectorWheel(false);
        distanceAfterComa.setMaxValue(9);
        distanceAfterComa.setMinValue(0);
        okTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                distanceEdit.setText(String.valueOf(distance.getValue())
                        + "," + (distanceAfterComa.getValue())
                        + " " + (spinnerDistance.getSelectedItem().toString()));
                dialog.dismiss();
            }
        });

        cancelTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}

