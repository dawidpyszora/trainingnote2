package com.example.dawid.notatnik2.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.dawid.notatnik2.R;

import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private TextView utworzNoweKonto;
    private Button zaloguj;
    private Button exit;
    Context context;

    private Button loginBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        utworzNoweKonto = (TextView) findViewById(R.id.crete_user);
        loginBut = (Button) findViewById(R.id.login_but);

        loginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(LoginActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        utworzNoweKonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginForm();
            }
        });
        exit = (Button) findViewById(R.id.exit_but);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void openLoginForm() {
        Intent intent = new Intent(getApplicationContext(), RegisterForrm.class);
        startActivity(intent);
    }
}
