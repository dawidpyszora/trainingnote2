package com.example.dawid.notatnik2.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.dawid.notatnik2.Database.TrainingsData;
import com.example.dawid.notatnik2.Database.UserData;
import com.example.dawid.notatnik2.Models.IntentConstants;
import com.example.dawid.notatnik2.Models.Training;
import com.example.dawid.notatnik2.Adapters.MyListAdapter;
import com.example.dawid.notatnik2.Models.User;
import com.example.dawid.notatnik2.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListActivity extends AppCompatActivity {

    private static final String DEBUG_TAR = "AppCompactActivity";

    @BindView(R.id.list_of_training)
    RecyclerView recyclerView;
    @BindView(R.id.floating_action_but)
    FloatingActionButton fab;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Intent intent;

    public ArrayList<User> users;
    public UserData userData  = new UserData(this);
    public ArrayList<Training> trainingsList;
    public MyListAdapter adapter;
    public TrainingsData trainingsData = new TrainingsData(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_training);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

//        new EditOrCrreateUserTask().execute();
        new GetTrainingListTask().execute();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setClass(ListActivity.this, EditTrainingData.class);
                startActivityForResult(intent, adapter.getItemCount());
            }
        });

        setUpItemTouchHelper();
    }

    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                showDeleteDialog(swipedPosition);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    public void showDeleteDialog(final int swipedPosition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete training")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        id = swipedPosition;
                        adapter.deleteTraining(id);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        adapter.notifyDataSetChanged();

                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {


        switch (item.getItemId()) {
            case R.id.sorted_by_date:
                Collections.sort(trainingsList, new Comparator<Training>() {
                    @Override
                    public int compare(Training o1, Training o2) {
                        return o2.getDate().compareTo(o1.getDate());
                    }
                });

                adapter.notifyDataSetChanged();
                Toast.makeText(this, "Sorted by date", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sorted_by_title:
                Collections.sort(trainingsList, new Comparator<Training>() {
                    @Override
                    public int compare(Training o1, Training o2) {
                        return o1.getTitle().compareTo(o2.getTitle());
                    }
                });

                adapter.notifyDataSetChanged();
                Toast.makeText(this, "Sorted by title", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == adapter.getItemCount()) {
            if (resultCode == RESULT_OK) {
                Training training = data.getParcelableExtra("trainingEdit");
                adapter.addTraining(training);
                adapter.notifyDataSetChanged();
            }
        } else if (resultCode == RESULT_OK) {
            Training training = data.getParcelableExtra("trainingEdit");
            adapter.editTraining(training, requestCode);
            adapter.notifyDataSetChanged();
        } else if (resultCode == IntentConstants.INTENT_REQUEST_DELETE) {
//            adapter.showDeleteDialog(requestCode);
            adapter.deleteTraining(requestCode);
        }

    }

    public void doSmoothScroll(int position) {
        recyclerView.smoothScrollToPosition(position);
    }

    public class EditOrCrreateUserTask extends AsyncTask<Void, Void, ArrayList<User>> {

        @Override
        protected ArrayList<User> doInBackground(Void... voids) {
            userData.open();
            users = userData.allUsers();
            return users;
        }

        @Override
        protected void onPostExecute(ArrayList<User> users) {
            Toast.makeText(ListActivity.this, "post Execuite ", Toast.LENGTH_SHORT).show();

        }
    }

    private class GetTrainingListTask extends AsyncTask<Void, Void, ArrayList<Training>> {
        @Override
        protected ArrayList<Training> doInBackground(Void... params) {
            trainingsData.open();
            trainingsList = trainingsData.getAllTraining();
            return trainingsList;
        }

        @Override
        protected void onPostExecute(ArrayList<Training> trainings) {
            super.onPostExecute(trainings);
            adapter = new MyListAdapter(ListActivity.this, trainingsList, trainingsData);
            recyclerView.setAdapter(adapter);
        }
    }


}
