package com.example.dawid.notatnik2.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dawid on 2017-09-16.
 */

public class Training  implements Parcelable{
    private long id;
    private String date;
    private String title;
    private String distance;
    private String timeOfRoute;
    private String  averageSpeed;
    private String  uphill;
    private String comment;
    private int listPosition =0;

    public Training() {

    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTimeOfRoute() {
        return timeOfRoute;
    }

    public void setTimeOfRoute(String timeOfRoute) {
        this.timeOfRoute = timeOfRoute;
    }

    public String getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(String averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public String getUphill() {
        return uphill;
    }

    public void setUphill(String uphill) {
        this.uphill = uphill;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getListPosition() {
        return listPosition;
    }

    public void setListPosition(int listPosition) {
        this.listPosition = listPosition;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(date);
        dest.writeString(title);
        dest.writeString(distance);
        dest.writeString(timeOfRoute);
        dest.writeString(averageSpeed);
        dest.writeString(uphill);
        dest.writeString(comment);
        dest.writeInt(listPosition);


    }
    public static final Parcelable.Creator<Training> CREATOR = new Parcelable.Creator<Training>(){

        public Training createFromParcel(Parcel source) {
            return new Training(source);
        }

        public Training[] newArray(int size) {
            return new Training[size];
        }
    };
    public Training(Parcel in) {
        id = in.readLong();
        date = in.readString();
        title = in.readString();
        distance = in.readString();
        timeOfRoute = in.readString();
        averageSpeed = in.readString();
        uphill = in.readString();
        comment = in.readString();
        listPosition = in.readInt();
    }
}
