package com.example.dawid.notatnik2.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dawid.notatnik2.Database.UserData;
import com.example.dawid.notatnik2.Models.User;
import com.example.dawid.notatnik2.R;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterForrm extends AppCompatActivity {

    @BindView(R.id.name_edt)
    EditText nameUser;
    @BindView(R.id.email_edt)
    EditText emailUser;
    @BindView(R.id.password_edt)
    EditText passwordUser;
    @BindView(R.id.password_repeat)
    EditText passwordUserRepead;
    @BindView(R.id.date_of_birth)
    Button dateOfBirth;
    @BindView(R.id.weight_edt)
    EditText weightUser;
    @BindView(R.id.high_edt)
    EditText highUser;
    @BindView(R.id.create)
    Button createButton;
    @BindView(R.id.cancel_butt)
    Button cancel_Button;


    Intent intent;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private ArrayList<User> userArrayList;
    private UserData userData;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_form);
        ButterKnife.bind(this);

        userData= new UserData(this);
        user = new User();


        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        RegisterForrm.this, android.R.style.Theme_Holo_Dialog_MinWidth,
                        mDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                String date = day + "-" + month + "-" + year;
                dateOfBirth.setText(date);
            }
        };

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (correctInputValidation() == true) {
                    creatUser();
                    Toast.makeText(RegisterForrm.this, "Congratulation crreate user!", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(RegisterForrm.this, "upppss", Toast.LENGTH_LONG).show();
            }
        });

    }

    public boolean correctInputValidation() {
        String name = nameUser.getText().toString().trim();
        String email = emailUser.getText().toString().trim();
        String password = passwordUser.getText().toString().trim();
        String password2 = passwordUserRepead.getText().toString().trim();
        String dateBirth = dateOfBirth.getText().toString().trim();
        if (name.isEmpty()) {
            Toast.makeText(this, "Pleas, add your name!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Emeil is uncorected!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.isEmpty() || (password.length() < 4) || (password.length() > 10)) {
            Toast.makeText(this, "Password shoud be beetween 4 and 10 alphanumeric charters", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!password.equals(password2)) {
            Toast.makeText(this, "Password and reaped password is uncorrected!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (dateBirth.isEmpty()) {
            Toast.makeText(this, "Pleas add your date of birth", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void creatUser() {
        User addUser = new User();
       addUser.setNameUser(nameUser.getText().toString().trim());
       addUser.setMail(emailUser.getText().toString().trim());
       addUser.setPassword(passwordUser.getText().toString().trim());
       addUser.setDateOfBirth(dateOfBirth.getText().toString().trim());
       addUser.setHight(highUser.getText().toString().trim());
       addUser.setWeight(weightUser.getText().toString().trim());

        new CreateUserTask().execute(addUser);
    }

private class CreateUserTask extends AsyncTask<User, Void, User>{

    @Override
    protected User doInBackground(User... users) {
        userData.creatUserInDb(users[0]);
        return users[0];
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        Intent intent = new Intent(RegisterForrm.this, ListActivity.class);
        startActivity(intent);
        finish();

    }
}
}

