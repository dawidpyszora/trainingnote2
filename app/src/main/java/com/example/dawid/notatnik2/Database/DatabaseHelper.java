package com.example.dawid.notatnik2.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Dawid on 2017-09-21.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "trainingsNote.db";
    private static final int DB_VERSION = 2;

    protected static final String TABLE_USER = "USER";
    protected static final String TABLE_TRAINING = "TRAININGS";

    protected static final String USER_ID = "USER_ID";
    protected static final String USER_NAME = "USER_NAME";
    protected static final String USER_MAIL = "USER_MAIL";
    protected static final String USER_PASSWORD = "USER_PASSWORD";
    protected static final String USER_DATE_OF_BIRTH = "USER_DATE_OF_BIRTH";
    protected static final String USER_WEIGHT = "USER_WEIGHT";
    protected static final String USER_HEIGHT = "USER_HEIGHT";

    protected static final String TRAINING_COLUMN_ID = "ID";
    protected static final String TRAINING_COLUMN_DATE = "DATE";
    protected static final String TRAINING_COLUMN_TITLE = "TITLE";
    protected static final String TRAINING_COLUMN_DISTANCE = "DISTANCE";
    protected static final String TRAINING_COLUMN_TIME_OF_ROUTE = "TIME_OF_ROUTE";
    protected static final String TRAINING_COLUMN_AVERAGE_SPEED = "AVERAGE_SPEED";
    protected static final String TRAINING_COLUMN_UPHILL = "UPHILL";
    protected static final String TRAINING_COLUMN_COMMENTS = "COMMENTS";
    protected static final String TRAINING_COLUMN_USER_ID = USER_ID;

    private static final String TABLE_USER_CREATE = "CREATE TABLE " + TABLE_USER + "(" +
            USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_NAME + " TEXT, " +
            USER_MAIL + " TEXT, " +
            USER_PASSWORD + " TEXT, " +
            USER_DATE_OF_BIRTH + " DATE, " +
            USER_WEIGHT + " NUMBER, " +
            USER_HEIGHT + " NUMBER " + ")";

    private static final String TABLE_TRAINING_CREATE = "CREATE TABLE " + TABLE_TRAINING + "(" +
            TRAINING_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TRAINING_COLUMN_DATE + " TEXT, " +
            TRAINING_COLUMN_TITLE + " TEXT, " +
            TRAINING_COLUMN_DISTANCE + " NUMBER, " +
            TRAINING_COLUMN_TIME_OF_ROUTE + " TEXT, " +
            TRAINING_COLUMN_AVERAGE_SPEED + " NUMBER, " +
            TRAINING_COLUMN_UPHILL + " NUMBER, " +
            TRAINING_COLUMN_COMMENTS + " TEXT, " +
            TRAINING_COLUMN_USER_ID + " INTEGER," +
            " FOREIGN KEY(" + USER_ID + ") REFERENCES " +
            TABLE_USER + "(USER_ID) " +
            ")";

//    private static DatabaseHelper instance;
//
//    public static synchronized DatabaseHelper getHelper(Context context) {
//        if (instance == null)
//            instance = new DatabaseHelper(context);
//        return instance;
//    }

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

//    @Override
//    public void onOpen(SQLiteDatabase db) {
//        super.onOpen(db);
//        if (!db.isReadOnly()) {
//            // Enable foreign key constraints
//            db.execSQL("PRAGMA foreign_keys=ON;");
//        }
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_USER_CREATE);
        db.execSQL(TABLE_TRAINING_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING);
        onCreate(db);
    }
}
