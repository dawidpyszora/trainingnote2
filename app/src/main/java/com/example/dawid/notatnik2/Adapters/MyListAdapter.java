package com.example.dawid.notatnik2.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dawid.notatnik2.Activity.EditTrainingData;
import com.example.dawid.notatnik2.Activity.ListActivity;
import com.example.dawid.notatnik2.Database.TrainingsData;
import com.example.dawid.notatnik2.Models.IntentConstants;
import com.example.dawid.notatnik2.Models.Training;
import com.example.dawid.notatnik2.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dawid on 2017-09-16.
 */

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
    private static final String DEBUG_TAG = "MyListAdapter";

    private ArrayList<Training> trainingsList;
    private Context context;
    private TrainingsData trainingsData;

    public MyListAdapter(Context context, ArrayList<Training> trainingsList, TrainingsData trainingsData) {
        this.context = context;
        this.trainingsList = trainingsList;
        this.trainingsData = trainingsData;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        String date = trainingsList.get(position).getDate();
        String title = trainingsList.get(position).getTitle();
        TextView dateTv = viewHolder.dateOfListHolder;
        TextView titleTV = viewHolder.titleOfListHolder;
        dateTv.setText(date);
        titleTV.setText(title);
    }

    public void addTraining(Training training) {

        Training trainingAdd= training;
        new CreateTrainingTask().execute(trainingAdd);
        notifyItemInserted(getItemCount());
    }

    public void deleteTraining(int listPosition) {
//        showDeleteDialog();
        Training training = new Training();
        training.setId(getItemId(listPosition));
        training.setListPosition(listPosition);
        new DeleteTrainingTask().execute(training);
    }

    public void editTraining(Training training, int listPosition){

        Training trainingEdit = training;
        trainingEdit.setId(getItemId(listPosition));
        trainingEdit.setListPosition(listPosition);
        new EditTrainingTask().execute(trainingEdit);
    }

    @Override
    public int getItemCount() {
        if (trainingsList.isEmpty()) {
            return 0;
        } else {
            return trainingsList.size();
        }
    }

    @Override
    public long getItemId(int position) {
        return trainingsList.get(position).getId();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        View v = layoutInflater.inflate(R.layout.item_of_list_view_layout, viewGroup, false);

        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_of_list) TextView dateOfListHolder;
        @BindView(R.id.title_of_list) TextView titleOfListHolder;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openEditActivity();

                }
            });
        }
        public void openEditActivity() {
            int position = getAdapterPosition();

            Intent intent = new Intent(context, EditTrainingData.class);
            intent.putExtra(IntentConstants.INTENT_DATE, trainingsList.get(position).getDate());
            intent.putExtra(IntentConstants.INTENT_TITLE, trainingsList.get(position).getTitle());
            intent.putExtra(IntentConstants.INTENT_DISTANCE, trainingsList.get(position).getDistance());
            intent.putExtra(IntentConstants.INTENT_TIME_OF_ROUTE, trainingsList.get(position).getDistance());
            intent.putExtra(IntentConstants.INTENT_AVERAGE_SPEED, trainingsList.get(position).getAverageSpeed());
            intent.putExtra(IntentConstants.INTENT_UPHILL, trainingsList.get(position).getUphill());
            intent.putExtra(IntentConstants.INTENT_COMMENTS, trainingsList.get(position).getComment());

            ((AppCompatActivity) context).startActivityForResult(intent, position);
        }
    }

    private class CreateTrainingTask extends AsyncTask<Training, Void, Training> {
        @Override
        protected Training doInBackground(Training... trainings) {
            trainingsData.createTraining(trainings[0]);
            trainingsList.add(trainings[0]);
            return trainings[0];
        }

        @Override
        protected void onPostExecute(Training training) {
            super.onPostExecute(training);
            ((ListActivity) context).doSmoothScroll(getItemCount() - 1);
            notifyItemInserted(getItemCount());
            Log.d(DEBUG_TAG, "utworzono trening o id " + training.getId() + " data " +
                    training.getDate() + " tytul " + training.getTitle());
        }
    }

    private class EditTrainingTask extends AsyncTask<Training, Void, Training> {
        @Override
        protected Training doInBackground(Training... trainings) {
            trainingsData.edit(trainings[0].getId(), trainings[0].getDate(), trainings[0].getTitle(),
                    trainings[0].getDistance(), trainings[0].getTimeOfRoute(), trainings[0].getAverageSpeed(),
                    trainings[0].getUphill(), trainings[0].getComment());
            trainingsList.get(trainings[0].getListPosition()).setDate(trainings[0].getDate());
            trainingsList.get(trainings[0].getListPosition()).setTitle(trainings[0].getTitle());
            trainingsList.get(trainings[0].getListPosition()).setDistance(trainings[0].getDistance());
            trainingsList.get(trainings[0].getListPosition()).setTimeOfRoute(trainings[0].getTimeOfRoute());
            trainingsList.get(trainings[0].getListPosition()).setAverageSpeed(trainings[0].getAverageSpeed());
            trainingsList.get(trainings[0].getListPosition()).setUphill(trainings[0].getUphill());
            trainingsList.get(trainings[0].getListPosition()).setComment(trainings[0].getComment());
            return trainings[0];
        }

        @Override
        protected void onPostExecute(Training training) {
            super.onPostExecute(training);
            Log.d(DEBUG_TAG, "list pozycjon :" + training.getListPosition());
            notifyItemChanged(training.getListPosition());
        }
    }

    private class DeleteTrainingTask extends AsyncTask<Training, Void, Training> {
        @Override
        protected Training doInBackground(Training... trainings) {
            trainingsData.delete(trainings[0].getId());
            trainingsList.remove(trainings[0].getListPosition());
            return trainings[0];

        }

        @Override
        protected void onPostExecute(Training training) {
            super.onPostExecute(training);
            notifyItemRemoved(training.getListPosition());
        }
    }
}
