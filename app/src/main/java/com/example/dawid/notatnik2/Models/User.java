package com.example.dawid.notatnik2.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dawid on 2017-11-01.
 */

public class User implements Parcelable {

    private long id;
    private String nameUser;
    private String mail;
    private String password;
    private String dateOfBirth;
    private String weight;
    private String hight;

    public User() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHight() {
        return hight;
    }

    public void setHight(String hight) {
        this.hight = hight;
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {


        public User createFromParcel(Parcel source) {
            return new User(source);
        }


        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(Parcel in) {
        id = in.readLong();
        nameUser = in.readString();
        mail = in.readString();
        password = in.readString();
        dateOfBirth =in.readString();
        weight = in.readString();
        hight = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nameUser);
        dest.writeString(mail);
        dest.writeString(password);
        dest.writeString(dateOfBirth);
        dest.writeString(weight);
        dest.writeString(hight);
    }
}
