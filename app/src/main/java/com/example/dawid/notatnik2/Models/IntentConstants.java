package com.example.dawid.notatnik2.Models;

/**
 * Created by Dawid on 2017-08-30.
 */

public class IntentConstants {

    public final static int INTENT_REQUEST_DELETE = 3;
    public final static int INTENT_RESULT_DELETE = 3;

    public final static String INTENT_DATE = "date";
    public final static String INTENT_TITLE = "title";
    public final static String INTENT_DISTANCE = "distance";
    public final static String INTENT_TIME_OF_ROUTE = "time_of_route";
    public final static String INTENT_AVERAGE_SPEED = "average_speed";
    public final static String INTENT_UPHILL = "uphill";
    public final static String INTENT_COMMENTS = "comments";





}
