package com.example.dawid.notatnik2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.dawid.notatnik2.Models.User;

import java.util.ArrayList;

import static com.example.dawid.notatnik2.Database.DatabaseHelper.TABLE_USER;

/**
 * Created by Dawid on 2017-11-02.
 */

public class UserData {
    public static final String DEBUG_TAG = "UserData";

    private SQLiteDatabase db;
    private SQLiteOpenHelper userDbHelper;

    private static final String[] ALL_COLLUMNS = {
            DatabaseHelper.USER_ID,
            DatabaseHelper.USER_NAME,
            DatabaseHelper.USER_MAIL,
            DatabaseHelper.USER_PASSWORD,
            DatabaseHelper.USER_DATE_OF_BIRTH,
            DatabaseHelper.USER_WEIGHT,
            DatabaseHelper.USER_HEIGHT,
    };

    public UserData(Context context) {
        this.userDbHelper = new DatabaseHelper(context);
    }

    public void open() {
        db = userDbHelper.getWritableDatabase();
    }

//    public ArrayList<User> getAllUsser (){
//        ArrayList<User> users = new ArrayList<>();
//        Cursor cursor = null;
//        cursor = db.query()
//    }

    public User creatUserInDb(User user) {
        open();
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.USER_NAME, user.getNameUser());
        values.put(DatabaseHelper.USER_MAIL, user.getMail());
        values.put(DatabaseHelper.USER_PASSWORD, user.getPassword());
        values.put(DatabaseHelper.USER_DATE_OF_BIRTH, user.getDateOfBirth());
        values.put(DatabaseHelper.USER_WEIGHT, user.getWeight());
        values.put(DatabaseHelper.USER_HEIGHT, user.getHight());

        long idUser = db.insert(TABLE_USER, null, values);
        user.setId(idUser);

        return user;
    }

    public ArrayList<User> allUsers() {

        ArrayList<User> users = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(TABLE_USER, ALL_COLLUMNS, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    User user = new User();
                    user.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.USER_ID)));
                    user.setNameUser(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_NAME)));
                    user.setMail(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_MAIL)));
                    user.setPassword(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_PASSWORD)));
                    user.setDateOfBirth(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_DATE_OF_BIRTH)));
                    user.setWeight(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_WEIGHT)));
                    user.setHight(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_HEIGHT)));
                    users.add(user);

                }
            }
        } catch (Exception e) {
            Log.d(DEBUG_TAG, "exception " + e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return users;
    }
}
