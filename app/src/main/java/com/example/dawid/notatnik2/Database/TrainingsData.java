package com.example.dawid.notatnik2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.dawid.notatnik2.Models.Training;

import java.util.ArrayList;

import static com.example.dawid.notatnik2.Database.DatabaseHelper.TABLE_TRAINING;

/**
 * Created by Dawid on 2017-09-21.
 */

public class TrainingsData {

    public static final String DEBUG_TAG = "TrainingsData";

    private SQLiteDatabase db;
    private SQLiteOpenHelper trainingDbHelper;

    private static final String[] ALL_COLUMNS = {
            DatabaseHelper.TRAINING_COLUMN_ID,
            DatabaseHelper.TRAINING_COLUMN_DATE,
            DatabaseHelper.TRAINING_COLUMN_TITLE,
            DatabaseHelper.TRAINING_COLUMN_DISTANCE,
            DatabaseHelper.TRAINING_COLUMN_TIME_OF_ROUTE,
            DatabaseHelper.TRAINING_COLUMN_AVERAGE_SPEED,
            DatabaseHelper.TRAINING_COLUMN_UPHILL,
            DatabaseHelper.TRAINING_COLUMN_COMMENTS,
    };

    public TrainingsData(Context context) {
        this.trainingDbHelper = new DatabaseHelper(context);
    }

    public void open() {
        db = trainingDbHelper.getWritableDatabase();
    }

    public ArrayList<Training> getAllTraining() {

        ArrayList<Training> trainings = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(TABLE_TRAINING, ALL_COLUMNS, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    Training training = new Training();
                    training.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_ID)));
                    training.setDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_DATE)));
                    training.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_TITLE)));
                    training.setDistance(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_DISTANCE)));
                    training.setTimeOfRoute(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_TIME_OF_ROUTE)));
                    training.setAverageSpeed(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_AVERAGE_SPEED)));
                    training.setUphill(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_UPHILL)));
                    training.setComment(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRAINING_COLUMN_COMMENTS)));
                    trainings.add(training);
                }
            }
        } catch (Exception e) {
            Log.d(DEBUG_TAG, "zgłoszono wyjątek" + e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return trainings;
    }

    public Training createTraining(Training training) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TRAINING_COLUMN_DATE, training.getDate());
        values.put(DatabaseHelper.TRAINING_COLUMN_TITLE, training.getTitle());
        values.put(DatabaseHelper.TRAINING_COLUMN_DISTANCE, training.getDistance());
        values.put(DatabaseHelper.TRAINING_COLUMN_TIME_OF_ROUTE, training.getTimeOfRoute());
        values.put(DatabaseHelper.TRAINING_COLUMN_AVERAGE_SPEED, training.getAverageSpeed());
        values.put(DatabaseHelper.TRAINING_COLUMN_UPHILL, training.getUphill());
        values.put(DatabaseHelper.TRAINING_COLUMN_COMMENTS, training.getComment());

        long id = db.insert(TABLE_TRAINING, null, values);
        training.setId(id);

        return training;
    }

    public void edit(long id, String date, String title, String distance, String timeOfRoute,
                     String averageSpeed, String uphill, String comment) {
        String whereClause = DatabaseHelper.TRAINING_COLUMN_ID + "=" + id;
        Log.d(DEBUG_TAG, "identyfikatro aktualizowanego obiektu :" + String.valueOf(id));
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TRAINING_COLUMN_DATE, date);
        values.put(DatabaseHelper.TRAINING_COLUMN_TITLE, title);
        values.put(DatabaseHelper.TRAINING_COLUMN_DISTANCE, distance);
        values.put(DatabaseHelper.TRAINING_COLUMN_TIME_OF_ROUTE, timeOfRoute);
        values.put(DatabaseHelper.TRAINING_COLUMN_AVERAGE_SPEED, averageSpeed);
        values.put(DatabaseHelper.TRAINING_COLUMN_UPHILL, uphill);
        values.put(DatabaseHelper.TRAINING_COLUMN_COMMENTS, comment);
        db.update(TABLE_TRAINING, values, whereClause, null);
    }

    public void delete(long id) {
        String whereClause = DatabaseHelper.TRAINING_COLUMN_ID + "=" + id;
        db.delete(DatabaseHelper.TABLE_TRAINING, whereClause, null);
    }
}
